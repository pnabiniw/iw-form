# IW Form

Django Project to collect information of all the students of IW Academy Batch II.

Implementations:
- User Authentication System.
- Class Based Views. (DetailView and ListView)
- CRUD system.
