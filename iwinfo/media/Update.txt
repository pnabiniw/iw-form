* Todays Update [25th April, 2019] *
- Learnt about python functions and loops
- Completed first assignment of python

* Today's Update [26th April, 2019] *
- Completed second assignment of python and started third assignment.
- Guest lecture on software project management

* Today's Update [29th April, 2019]: *
- Continuing Python assignment 3.
- Started slack pages design.
- Studied about lambda functions, reduce, map, filter, file handling, list comprehension in python.
- SSH key added to gitlab repo.


* Today's Update [30th April, 2019]: *
- Continuing Python Assignment 3.
- Slack sign in page and invite members page design completed.
- On going slack manage members page design.
- SQLITE database with python.

* Today's Update [1st May, 2019]: *
- Completed python assignment 4.
- Ongoing python assignment 3.
- Ongoing slack members management page design.
- Learnt basic concept of cloud computing and some keywords.  

* Today's Update [2nd May, 2019]: *
- Research on python decorators and closures.
- Learnt and implemented simple test cases in python.
- Ongoing slack page design.

* Today's Update [3rd May, 2019]: *
- Research on python generators, lambda functions, list-comprehensions.
- Python exam.
- Research on unsolved problems of exam.
- Lab Session- Implemented test cases in python.

* Today's Update [6 May, 2019]: *
- Ongoing slack page design. 
- Started Django Project and apps in Django.
- Learnt about MVC and MVT.
- Learnt about virtual environment.
- Project structure of Django.
- Load static files in Django.


* Today's Update [7 May, 2019]: *
- Simple weather app using django.
- Mysql database setup for django.
- Rendered slack pages using django.
- Learnt about .env files and gitignore. 


* Today's Update [8 May, 2019]: *
- Review on some topics of Python (Decorators, generators, iterators).
- On going weather app.
- Learnt about databses and django models.
- learnt about migrations.
- Created a superuser in to access django admin.


* Today's Update [9 May, 2019]: *
- Completed weather app.
- Learnt about SQL queries.
- Learnt about Django ORM.
- Populated database with faker.


* Today's Update [10 May, 2019]: *
- Reserch on django ORM.
- Learnt to read data from database and send it to html by using context dictionary.
- Learnt about GET and POST resquests.


* Today's Update [13 May, 2019]: *
- Learnt to post data from html to database.
- Upload images to media folder and use it in html. 
- Reserach on django views (class and functions based)


* Today's Update [14 May, 2019]: *
- Research on Django views.
- A simple CRUD app in progress.

* Today's Update [15 May, 2019]: *
- Research on django authentication system.
- The CRUD implementation completed. User authentication part remaining. 


* Today's Update [17 May, 2019]: *
- Authentication system (login, logout) completed.
- User registration system in progress.
- Research on class based views.





















