from django.contrib.auth.decorators import login_required
from django.urls import path
from . import views
from .views import TableView, ApplicantDetailView


urlpatterns = [
    path('', views.apply_form, name='apply_form'),
    path('list_view/', login_required(TableView.as_view()), name='list_view'),
    path('delete/<int:id>/', views.delete_record, name='delete'),
    path('detail/<int:pk>', ApplicantDetailView.as_view(), name='detail'),
    path('edit_form/<int:id>', views.edit_form, name='edit_form')
]
