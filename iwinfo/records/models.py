from django.db import models


class Application(models.Model):
    name = models.CharField(max_length=40)
    email = models.EmailField()
    number = models.PositiveIntegerField()
    gender = models.CharField(max_length=10)
    file = models.FileField(upload_to='CVs', blank=True)
    motivation = models.TextField()

    def __str__(self):
        return self.name
