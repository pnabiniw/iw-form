from django.shortcuts import render, redirect
from django.core.files.storage import FileSystemStorage
from . models import Application
from django.contrib import messages
from django.views.generic import ListView, DetailView


def apply_form(request):
    if request.method == 'POST':
        name = request.POST['full_name']
        email = request.POST['email']
        number = request.POST['phone_number']
        phone = int(number)
        gender = request.POST['gender']

        file = request.FILES['my_file']
        fs = FileSystemStorage(base_url="")
        filename = fs.save(file.name, file)
        file = fs.url(filename)

        motivation = request.POST['motivation']
        a = Application(name=name, email=email, number=phone, gender=gender, file=file, motivation=motivation)
        a.save()
        messages.success(request, f'Your form is Submitted.')
        return redirect('apply_form')

    else:
        return render(request, 'records/apply_form.html')


class TableView(ListView):
    model = Application
    template_name = 'records/list.html'


class ApplicantDetailView(DetailView):
    template_name = 'records/detail.html'
    model = Application


def edit_form(request, id):
    if request.method == 'POST':
        name = request.POST['full_name']
        email = request.POST['email']
        number = request.POST['phone_number']
        phone = int(number)
        gender = request.POST['gender']

        file = request.FILES['my_file']
        fs = FileSystemStorage(base_url="")
        filename = fs.save(file.name, file)
        file = fs.url(filename)

        motivation = request.POST['motivation']

        a = Application.objects.filter(id=id)
        a.update(name=name, email=email, number=phone, gender=gender, file=file, motivation=motivation)
        messages.success(request, f'Updated Successfully.')
        return redirect('list_view')

    else:
        a = Application.objects.get(id=id)
        return render(request, 'records/update_form.html', {'a': a})


def delete_record(request, id):
    Application.objects.get(id=id).delete()
    return redirect('list_view')
